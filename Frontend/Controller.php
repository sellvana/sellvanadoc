<?php

class FCom_SellvanaDoc_Frontend_Controller extends FCom_Frontend_Controller_Abstract
{
    public function action_index()
    {
        $this->layout('/fdoc');
    }

    public function action_section()
    {
        $area = $this->BRequest->param('area');
        $pageName = $this->viewProxy('fdoc/'.$area, 'index', 'fdoc-section', '/fdoc/section');
        if (!$pageName) {
            $this->forward('noroute');
            return;
        }
        $this->BLayout->hookView('main', 'fdoc/'.$area);
    }

    public function action_noroute()
    {
        $this->layout('/fdoc/404');
    }
}
