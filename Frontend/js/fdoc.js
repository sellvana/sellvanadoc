define(['jquery', 'bootstrap', 'highlight'], function($) {
    hljs.initHighlightingOnLoad()

    var navHeight = $('.navbar').outerHeight(true) + 10;
    $(document.body).scrollspy({
        target: '#fdoc-navbar',
        offset: navHeight
    })

    var $sideBar = $('#fdoc-navbar');
    $sideBar.affix({
        offset: {
            top: $sideBar.offset().top,
            bottom: $('.f-site-footer').outerHeight(true)
        }
    });

    var section = location.href.match(/.*\/fdoc\/(buckyball|fulleron)\/([a-z]+).*/);
    if (section) {
        $('#nav-section-' + section[2]).addClass('active')
    }
})
